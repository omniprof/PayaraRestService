package com.kenfogel.tester;

import javax.ws.rs.core.Application;

/**
 * At its simplest the presence of this file with nothing in it signifies that
 * the server should scan every class file looking for web services
 *
 * @author Ken Fogel
 */
@javax.ws.rs.ApplicationPath("")
public class ApplicationConfig extends Application { }
