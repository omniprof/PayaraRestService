package com.kenfogel.tester;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JavaBean class for demonstrating how objects are managed by a REST service
 *
 * @author Ken Fogel
 */
// Required to allow this object to be represented as XML
// Not required if object will only be represented as JSON
@XmlRootElement
public class ResponseObject implements Serializable {

    private int num1;
    private int num2;
    private int result;

    public ResponseObject(int num1, int num2, int result) {
        this.num1 = num1;
        this.num2 = num2;
        this.result = result;
    }

    public ResponseObject() {
        this.num1 = 0;
        this.num2 = 0;
        this.result = 0;
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
