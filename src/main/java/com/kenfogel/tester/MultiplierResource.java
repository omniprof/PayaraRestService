package com.kenfogel.tester;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Samples of REST services
 *
 * @author Ken Fogel
 */
@Path("")
public class MultiplierResource {

    /**
     * Example using parameters in the path and returning a primitive type as text/plain
     *
     * @param x
     * @param y
     * @return
     */
    @GET
    @Path("multiply1/{num1}/{num2}")
    public int multiply1(@PathParam("num1") int x, @PathParam("num2") int y) {
        return (x * y);
    }

    /**
     * Example using the parameters in the query string and returning a
     * primitive type as text/plain
     *
     * @param x
     * @param y
     * @return
     */
    @GET
    @Path("multiply2")
    public int multiply2(@QueryParam("num1") int x, @QueryParam("num2") int y) {
        return (x * y);
    }

    /**
     * Example using parameters in the path and returning an object encoded as
     * per the MediaType in @Produces - Default is XML.
     *
     * @param x
     * @param y
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    // @Consumes expects the request to have a body such as in an @POST whereas 
    // an @GET does not have a body so the @Consumes is ignored.
    //@Consumes(MediaType.APPLICATION_XML)
    
    @Path("multiply3/{num1}/{num2}")
    public ResponseObject multiply3(@PathParam("num1") int x, @PathParam("num2") int y) {
        ResponseObject ro = new ResponseObject(x, y, x * y);
        return ro;
    }
}
